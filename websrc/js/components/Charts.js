/*globals React */

var React = require('react/addons');

var Charts = function(spec)
{
	var that = {};

	that.renderRightChildrenIntoRows = function(child)
	{
		// Push rendered children into a row array. 
		// Use a new row every time we exceed the max
		// number of cells (which is 12)
		if (!child.props.main)
		{
			row = this.rows[this.rows.length - 1];
			// Define our unique key, push on the child and if
			// we exceed the grid size, create a new row
			child.props.key = "cell-idi-" + row.length;
			row.push(<div key={child.props.key}>{child}</div>);
			this.width = this.width + child.props.widthInCells;
			if (this.width >= 12)
			{
				this.rows.push([]);
				this.width = 0;
			}
		}
	};

	that.renderRightSide = function() 
	{
		var r = 0;
		var rowElements = [];

		// Initial conditions, no cells and no rows
		this.width = 0;
		this.rows = [[]];

		// Populate rows with arrays of rendered cells
		React.Children.map(this.props.children, this.renderRightChildrenIntoRows, this);

		// Render the entire row with a unique key 
		for (r = 0; r < this.rows.length; ++r)
		{
			if (this.rows[r].length > 0)
			{
				rowElements.push(React.createElement("div", {className: "row", key: "row-id-" + r}, this.rows[r]));
			}
		}
		// Return the whole column
		return React.createElement("div", {className: "col-md-8"}, rowElements);
	};

	that.renderLeftChild = function(child)
	{
		if (child.props.main)
		{
			child.props.key = 'row-id-0';
			return (<div>{child}</div>);
		}
	}

	that.renderLeftSide = function() 
	{
		return (
		<div className="col-md-4">
			<div className='row'>
				{ React.Children.map(this.props.children, this.renderLeftChild, this) }
			</div>
		</div>
		);
	};
	
	that.render = function()
	{
		return (
		<div className='container-fluid'>
			<div className='row'>
				{ this.renderLeftSide() }
				{ this.renderRightSide() }
			</div>
		</div>
		);
	};

	return React.createClass(that);
}

module.exports = Charts;
