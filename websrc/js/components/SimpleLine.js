/*globals React */

React = require('react');
$ = require('jquery');
window.jQuery = $;
require('highstock');
require('highcharts');
ChartMixin = require('./ChartMixin.js')

///////////////////////

var SimpleLine = function(spec)
{
	var that = {};

	that.mixins = [ChartMixin({})],

	that.createSeries = function()
	{
		var i = 0;
		var epochSeconds = new Date().getTime();

		series = [{
		    name: 'Values',
		    type: 'line',
		    data: [],
		    tooltip: {
			valueSuffix: ' ' + this.props.units
		    }
		}];

		for (i = 0; i < 20; ++i)
		{
			series[0].data.push({x: epochSeconds + 3600*i*1000, y: 870 + Math.random()*200});
		}

		return series;
	},

	that.createXAxis = function()
	{
		var xAxis = [
		{
			type: 'datetime'
		}
		];

		return xAxis;
	},

	that.createYAxis = function()
	{
		var axis = [
		{
			tickInterval: this.props.tickInterval,
			min: this.props.min,
			max: this.props.max,
			labels:
			{
				format: '{value} ' + this.props.units,
				style: { color: Highcharts.getOptions().colors[0] }
			},
			title: { enabled: false }
		}
		];

		return axis;
	},

	that.componentDidMount = function()
	{
		var i = 0;
		var yAxis = this.createYAxis();
		var xAxis = this.createXAxis();
		var series = this.createSeries();

		this.chart = new Highcharts.Chart(
		{
			chart: {
				renderTo: this.refs.chart.getDOMNode(),
				zoomType: 'xy',
				spacingBottom: 0,
				spacingTop: 0,
				spacingLeft: 0,
				spacingRight: 0
			},
			title: {
				text: this.props.title,
				style: { fontSize: "14px" }
			},
			xAxis: xAxis,
			yAxis: yAxis,
			tooltip: {
				shared: true
			},
			legend: {
				enabled: false
			},
			series: series,
			credits: false
		});
	}

	that.render = function()
	{
		return this.renderCell();
	}

	that.renderChart = function(key, style)
	{
		return (
				<div key={key} style={style} ref="chart">
				</div>
			);
	}

	return React.createClass(that);
}

module.exports = SimpleLine;
