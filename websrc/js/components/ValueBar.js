/*globals React */

React = require('react');
$ = require('jquery');
window.jQuery = $;
require('highstock');
require('highcharts');
ChartMixin = require('./ChartMixin.js')

///////////////////////

var ValueBar = function(spec)
{
	var that = {};

	that.mixins = [ChartMixin({})],

	that.createSeries = function()
	{
		var cats = this.props.categories;
		var i = 0;

		series = [{
		    name: 'Values',
		    type: 'column',
		    data: [],
		    tooltip: {
			valueSuffix: ' ' + this.props.units
		    }
		}];

		for (i = 0; i < cats.length; ++i)
		{
			series[0].data.push(Math.random()*100);
		}

		return series;
	},

	that.createXAxis = function()
	{
		var xAxis = [{ categories: this.props.categories }];

		return xAxis;
	},

	that.createYAxis = function()
	{
		var axis = [
		{
			tickInterval: this.props.tickInterval,
			min: this.props.min,
			max: this.props.max,
			labels:
			{
				format: '{value} ' + this.props.units,
				style: { color: Highcharts.getOptions().colors[0] }
			},
			title: { enabled: false }
		}
		];

		return axis;
	},

	that.componentDidMount = function()
	{
		var i = 0;
		var yAxis = this.createYAxis();
		var xAxis = this.createXAxis();
		var series = this.createSeries();

		this.chart = new Highcharts.Chart(
		{
			chart: {
				renderTo: this.refs.chart.getDOMNode(),
				zoomType: 'xy',
				spacingBottom: 0,
				spacingTop: 0,
				spacingLeft: 0,
				spacingRight: 0
			},
			title: {
				text: this.props.title,
				style: { fontSize: "14px" }
			},
			xAxis: xAxis,
			yAxis: yAxis,
			tooltip: {
				shared: true
			},
			legend: {
				enabled: false
			},
			series: series,
			credits: false
		});
	}

	that.render = function()
	{
		return this.renderCell();
	}

	that.renderChart = function(key, style)
	{
		return (
				<div key={key} style={style} ref="chart">
				</div>
			);
	}

	return React.createClass(that);
}

module.exports = ValueBar;
