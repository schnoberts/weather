/*global document, React, $ */

var React = require('react');
var RouterMixin = require('react-mini-router').RouterMixin;

var Header = require('./Header.js');
var Charts = require('./Charts.js');
var Chart = require('./Chart.js');
var Compass = require('./Compass.js');
var ValueGauge = require('./ValueGauge.js');
var ValueBar = require('./ValueBar.js');
var SimpleLine = require('./SimpleLine.js');

var App = function(spec)
{
	var HEADER = Header({});
	var CHARTS = Charts({});
	var CHART = Chart({});
	var COMPASS = Compass({});
	var VALUEGUAGE = ValueGauge({});
	var VALUEBAR = ValueBar({});
	var SIMPLELINE = SimpleLine({});

	var that = {};

	that.mixins = [RouterMixin];

	that.routes = {
		'/': 'home',
		'/LiveCharts': 'home',
		'/HistoricalCharts': 'historicalCharts',
		'/Downloads': 'downloads',
		'/About': 'about',
		'/Contact': 'contact'
	};

	that.render = function()
	{
		return this.renderCurrentRoute();
	}

	that.home = function() 
	{
		return (
				<div>
					<HEADER/>
					<CHARTS>
						<CHART main={true} backgroundColor='orange' height='600px'/>

						<COMPASS title='Wind Direction' widthInCells={3}/>
						<VALUEBAR widthInCells={6}
							  categories = {['OUT', 'HEAT', 'CHILL', 'DEW', 'IN']}
							  title = 'Temperature'
							  units = '&deg;C'
							  min = {0}
							  max = {100}
							  tickInterval = {10}
						/>
						<VALUEGUAGE title='Wind Speed' units='mph' tickInterval={10} min={0} max={100} widthInCells={3} bands={{good: [0, 30, 'green'], ok: [31, 50, 'orange'], bad: [51, 100, 'red']}}/>

						<CHART backgroundColor='green' widthInCells={3}/>
						<SIMPLELINE widthInCells={6}
							  title = 'Barometric Pressure'
							  units = ' hPa'
							  min = {850}
							  max = {1100}
							  tickInterval = {10} />
						<CHART backgroundColor='red' widthInCells={3}/>

						<VALUEGUAGE title='Solar Radiation' 
							    units='W/m&sup2;'
							    min={0} 
							    max={1000}
							    tickInterval={100}
							    widthInCells={3} />
						<VALUEBAR widthInCells={3}
							  categories = {['IN', 'OUT']}
							  title = 'Humidity'
							  units = '%'
							  min = {0}
							  max = {100}
							  tickInterval = {10} />
						<CHART backgroundColor='red' widthInCells={3}/>
						<VALUEGUAGE title='UV' 
							    units='index'
							    min={0} 
							    max={16}
							    tickInterval={2}
							    widthInCells={3} />
					</CHARTS>
				</div>
			   );
	}

	that.downloads = function()
	{
		return (
				<div>
					<HEADER/>
					<div>DOWNLOADS</div>
				</div>
		       );
	}

	that.historicalCharts = function()
	{
		return (
				<div>
					<HEADER/>
					<div>HISTORICAL CHARTS</div>
				</div>
		       );
	}

	that.contact = function()
	{
		return (
				<div>
					<HEADER/>
					<div>CONTACT</div>
				</div>
		       );
	}

	that.about = function() 
	{
		return (
				<div>
					<HEADER/>
					<div>ABOUT</div>
				</div>
		       );
	}

	that.notFound = function(path) 
	{
		return <div className="not-found">Page Not Found: {path}</div>;
	}

	return React.createClass(that); 
};

module.exports = App;

