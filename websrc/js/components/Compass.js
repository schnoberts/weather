/*globals React */

React = require('react');
$ = require('jquery');
window.jQuery = $;
require('highstock');
require('highcharts');
ChartMixin = require('./ChartMixin.js')

///////////////////////

var Compass = function(spec)
{
	var that = { units: '\xB0' };

	that.mixins = [ChartMixin({})],

	that.componentDidMount = function()
	{
		this.chart = new Highcharts.Chart(
		{
			chart: 
			{
				renderTo: this.refs.chart.getDOMNode(),
				type: 'gauge',
                                marginBottom: 0,
                                spacingTop: 0,
                                marginLeft: 0,
                                marginRight: 0
			},
			pane:
			{
				startAngle: 0,
				endAngle: 360,
				borderWidth: 0

			},
			plotOptions:
			{
				gauge: 
				{
					dial:
					{
						baseWidth: 5
					},
					pivot: 
					{
						radius: 0
					}
				}
			},
			title: 
			{
				text: this.props.title,
				style: { fontSize: "14px" }
			},
			yAxis: 
			[
				{
					min: 0,
					max: 360,
					tickWidth: 2,
					tickLength: 20,
					tickInterval: 45,
					labels: 
					{
						rotation: 'auto',
						formatter: function()
						{
							if(this.value == 360) { return 'N'; }
							else if(this.value == 45) { return 'NE'; }
							else if(this.value == 90) { return 'E'; }
							else if(this.value == 135) { return 'SE'; }
							else if(this.value == 180) { return 'S'; }
							else if(this.value == 225) { return 'SW'; }
							else if(this.value == 270) { return 'W'; }
							else if(this.value == 315) { return 'NW'; }
						}
					}
				}
			],
			series: 
			[
				{
					name: 'Direction',
					data: [80],
                                        dataLabels:
                                        {
                                                formatter: function() {}
                                        }					
				}
			],
			tooltip: { valueSuffix: ' ' + this.units },
			credits: false
		});
	}

	that.render = function()
	{
		this.state.value = '80 ' + this.units;
		return this.renderCell()
	}

	that.renderChart = function(key, style)
	{
		return (
				<div key={key} style={style} ref="chart">
				</div>
			);
	}

	return React.createClass(that);
}

module.exports = Compass;
