/*globals React */

React = require('react');

///////////////////////

var ChartMixin = function(spec)
{
	var that = {};

	that.getInitialState = function() 
	{
		return { value: '' };
	}

	that.getDefaultProps = function()
	{
		return {
			backgroundColor: 'white',
			height: '200px',
			widthInCells: 3,
			main: false
		};
	}

	that.renderCell = function()
	{
		var style = { height: this.props.height, backgroundColor: this.props.backgroundColor }
		var className = 'col-md-' + (this.props.main ? 12 : this.props.widthInCells);
		var value = this.state.value === null ? 'None' : this.state.value;

		// We expect a key on props here since we likely to be created dynamically
		// and all sibling elements must have a unique ascii key (numbers don't work as
		// sort order isn't defined)
		return (
			<div key={this.props.key} className={className}>
				{this.renderChart(this.props.key, style)}
				<div ref='value' style={{textAlign: 'center'}}>{value}</div>
			</div>
		 
			);
	}

	return that;
}

module.exports = ChartMixin;
