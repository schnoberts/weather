/*globals React */

React = require('react');
$ = require('jquery');

///////////////////////

var Link = function(spec)
{
	var that = {};

	that.computeClassName = function()
	{
		if (window.location.href.indexOf(this.props.href) > 0)
		{
			return "active";
		}
		else
		{
			return "inactive"; 
		}
	}

	that.render = function()
	{
		// Render is going to be called every time we switch page, so we don't need state here
		// we can just use the current windows href as a guide.
		return (
			<li className={this.computeClassName()} onClick={this.onClick}>
				<a href={this.props.href}>{this.props.text}</a>
			</li>
		       );
	}

	return React.createClass(that);
}

var LINK=Link({});

///////////////////////

var Header = function(spec)
{
	var that = {};

	that.render = function() 
	{
		return (
		<nav className="navbar navbar-default navbar-static-top" role="navigation">
			<div className="container">
				<div className="navbar-header">
					<button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
						<span className="sr-only">Toggle navigation</span>
						<span className="icon-bar"></span>
						<span className="icon-bar"></span>
						<span className="icon-bar"></span>
					</button>
					<a className="navbar-brand" href="/LiveCharts">Birchington Weather</a>
				</div>
				<div id="navbar" className="navbar-collapse collapse">
					<ul className="nav navbar-nav">
						<LINK href="/LiveCharts" text="Home"/>
						<LINK href="/HistoricalCharts" text="Historical Charts"/>
						<LINK href="/Downloads" text="Downloads"/>
					</ul>
					<ul className="nav navbar-nav navbar-right">
						<LINK href="/About" text="About"/>
						<LINK href="/Contact" text="Contact"/>
					</ul>
				</div>
			</div>
		</nav>
		);
	}

	return React.createClass(that);
}

module.exports = Header;
