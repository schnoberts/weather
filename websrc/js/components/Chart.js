/*globals React */

React = require('react');

///////////////////////

var Chart = function(spec)
{
	var that = {};

	that.getDefaultProps = function()
	{
		return {
			backgroundColor: 'black',
			height: '200px',
			widthInCells: 3,
			main: false
		};
	}

	that.render = function()
	{
		style = { height: this.props.height, backgroundColor: this.props.backgroundColor }
		className = 'col-md-' + (this.props.main ? 12 : this.props.widthInCells);

		// We expect a key on props here since we likely to be created dynamically
		// and all sibling elements must have a unique ascii key (numbers don't work as
		// sort order isn't defined)
		return (
			<div key={this.props.key} className={className}>
				<div key={this.props.key} style={style}></div>
			</div>
		 
			);
	}

	return React.createClass(that);
}

module.exports = Chart;
