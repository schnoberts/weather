/*globals React */

React = require('react');
$ = require('jquery');
window.jQuery = $;
require('highstock');
require('highcharts');
ChartMixin = require('./ChartMixin.js')

///////////////////////

var ValueGauge = function(spec)
{
	var that = {};

	that.mixins = [ChartMixin({})],

	that.componentDidMount = function()
	{
		var bands = undefined;
		if (this.props.bands)
		{
			bands = [
				{
					color: this.props.bands.good[2],
					from: this.props.bands.good[0],
					to: this.props.bands.good[1]
				},
				{
					color: this.props.bands.ok[2],
					from: this.props.bands.ok[0],
					to: this.props.bands.ok[1]
				},
				{
					color: this.props.bands.bad[2],
					from: this.props.bands.bad[0],
					to: this.props.bands.bad[1]
				},
			]
		}

		this.chart = new Highcharts.Chart(
		{
			chart: 
			{
				renderTo: this.refs.chart.getDOMNode(),
				type: 'gauge',
				marginBottom: 0,
				spacingTop: 0,
				marginLeft: 0,
				marginRight: 0
			},
			pane:
			{
				startAngle: -150,
				endAngle: 150
			},
			plotOptions:
			{
				gauge:
				{
					dial:
					{
						baseWidth: 3
					},
					pivot:
					{
						radius: 2
					},
					overshoot: 5
				}
			},
			title:
			{
				text: this.props.title,
				style: { fontSize: "14px" }
			},
			yAxis: 
			[
				{
					labels: { distance: 12, rotation: 'auto' },
					tickInterval: this.props.tickInterval,
					tickPosition: 'outside',
					min: this.props.min,
					max: this.props.max,
					plotBands: bands
				}
			],
			series: 
			[
				{
					name: this.props.title,
					dataLabels:
					{
						formatter: function() {}
					},
					data:
					[
						{
							id: 'max', y: 16, dial: {backgroundColor: 'gray'}
						},
						{
							id: 'current', y: 10, dial: {backgroundColor: 'black'}
						}
					]
				}
			],
			tooltip: { valueSuffix: ' ' + this.props.units },
			credits: false
		});
	}

	that.render = function()
	{
		this.state.value = '10 ' + this.props.units + ' (max=16)';
		return this.renderCell();
	}

	that.renderChart = function(key, style)
	{
		return (
				<div key={key} style={style} ref="chart">
				</div>
			);
	}

	return React.createClass(that);
}

module.exports = ValueGauge;
