/*globals React */

var React = require('react/addons');

var Grid = function(spec)
{
	var that = {};

	that.render = function()
	{
		var redCell = {height:'200px', backgroundColor:'red'};
		var blueCell = {height:'200px', backgroundColor:'blue'};
		var greenCell = {height:'200px', backgroundColor:'green'};
		var yellowCell = {height:'200px', backgroundColor:'yellow'};
		var cyanMain = {height:'800px', backgroundColor:'cyan'};

		return (
		<div className='container-fluid'>
			<div className='row'>
				<div className='col-md-4'>
					<div className='row'>
						<div className='col-md-12'>
							<div style={cyanMain}></div>
						</div>
					</div>
				</div>
				<div className='col-md-8'>
					<div className='row'>
						<div className='col-md-3'>
							<div style={redCell}></div>
						</div>
						<div className='col-md-6'>
							<div style={blueCell}></div>
						</div>
						<div className='col-md-3'>
							<div style={greenCell}></div>
						</div>
					</div>
					<div className='row'>
						<div className='col-md-3'>
							<div style={blueCell}></div>
						</div>
						<div className='col-md-3'>
							<div style={greenCell}></div>
						</div>
						<div className='col-md-3'>
							<div style={redCell}></div>
						</div>
						<div className='col-md-3'>
							<div style={yellowCell}></div>
						</div>
					</div>
					<div className='row'>
						<div className='col-md-3'>
							<div style={yellowCell}></div>
						</div>
						<div className='col-md-3'>
							<div style={blueCell}></div>
						</div>
						<div className='col-md-3'>
							<div style={greenCell}></div>
						</div>
						<div className='col-md-3'>
							<div style={redCell}></div>
						</div>
					</div>
					<div className='row'>
						<div className='col-md-3'>
							<div style={redCell}></div>
						</div>
						<div className='col-md-3'>
							<div style={yellowCell}></div>
						</div>
						<div className='col-md-3'>
							<div style={blueCell}></div>
						</div>
						<div className='col-md-3'>
							<div style={greenCell}></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		);
	}

	return React.createClass(that);
}

module.exports = Grid;
