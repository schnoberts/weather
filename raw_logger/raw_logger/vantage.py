import logging
import twisted.protocols.basic
from twisted.internet import reactor

#####################
ACK = chr(0x06)

class VantageProtocolState:
    ASLEEP = 0                  # The station maybe asleep
    WAKINGUP = 1                # We're sending the station wakeups and waiting for a response
    OK_TO_SEND = 2              # Woken up, ok to send a command
    STOPPED = 3                 # Something bad happened, we're stopped
    COMMAND_SENT = 4            # We sent a command, waiting for an ACK/NACK
    COMMAND_DONE_PENDING = 5    # We are waiting for a DONE back

#####################

class VantageCommandState: 
    DONE = 0                    # The command is complete.
    AGAIN = 1                   # The command had enough data but is expected another iteration
    NOT_ENOUGH_DATA = 2         # The command didn't have enough data, we need to read()

#####################

class StandardCommand:
    def __init__(self, protocol):
        self.protocol = protocol
        self.log = protocol.log
        self.client = protocol.client

    def handle_data(self, buff):
        # A normal response which normally goes: <CR><LF>OK<CR><LF>WHATEVER<CR><LF>
        ok_idx = buff.index('\n\rOK\n\r')

        # If there's not enough data for a full OK then return
        if (ok_idx < 0):
            return (buff, False)
       
        (buff, command_state) = self.handle_response(buff, ok_idx + 6)

        return (buff, command_state)

    def handle_response(self, buff, response_start_idx):
        response = buff[response_start_idx:]
        response_end_idx = response.find('\n\r')

        # If we don't have enough data for a properly terminated
        # response then just return
        if (response_end_idx < 0):
            return (buff, VantageCommandState.NOT_ENOUGH_DATA)

        response = response[:response_end_idx]

        command_state = self.execute(response)

        if (command_state == VantageCommandState.DONE):
            return (buff[response_start_idx + response_end_idx + 2:], command_state)
        else:
            return (buff, command_state)

#####################

class VER(StandardCommand):
    def __init__(self, protocol):
        StandardCommand.__init__(self, protocol)

    def execute(self, response):
        self.client.got_ver(self.protocol, response)
        return VantageCommandState.DONE

    def request(self, transport):
        self.log.debug(r'Sending VER\n')
        transport.write('VER\n')

#####################

class TEST(StandardCommand):
    def __init__(self, protocol):
        StandardCommand.__init__(self, protocol)

    def execute(self, response):
        self.client.got_test(self.protocol, response)
        return VantageCommandState.DONE

    def request(self, transport):
        self.log.debug(r'Sending TEST\n')
        transport.write('TEST\n')

    def handle_data(self, buff):
        if (buff[:6] == 'TEST\n\r'):
            self.execute(buff[:4])
            return (buff[6:], VantageCommandState.DONE)
        else:
            return (buff, VantageCommandState.NOT_ENOUGH_DATA)

#####################

class LOOP(StandardCommand):
    def __init__(self, protocol):
        StandardCommand.__init__(self, protocol)

    def execute(self, response):
        self.client.got_reading(self.protocol, response)

    def request(self, transport):
        self.log.debug(r'Sending LOOP 10\n')
        transport.write('LOOP 10\n')
        self.loops_to_go = 10


    def handle_data(self, buff):
        self.log.debug('LOOP %d' % self.loops_to_go)
        if (self.loops_to_go == 10):
            if (len(buff) < 100):
                return (buff, VantageCommandState.NOT_ENOUGH_DATA)

            if (buff[0] != ACK):
                raise Exception('Expected binary ACK for first LOOP response')
            else:
                buff = buff[1:]
        else:
            if (len(buff) < 99):
                return (buff, VantageCommandState.NOT_ENOUGH_DATA)

        response = buff[:99]

        self.execute(response)

        self.loops_to_go = self.loops_to_go - 1

        if (self.loops_to_go > 0):
            return (buff[99:], VantageCommandState.AGAIN)
        else:
            self.log.debug('LOOP DONE %d' % len(buff[99:]))
            return (buff[99:], VantageCommandState.DONE)

#####################

class NVER(StandardCommand):
    def __init__(self, protocol):
        StandardCommand.__init__(self, protocol)

    def execute(self, response):
        self.client.got_nver(self.protocol, response)
        return VantageCommandState.DONE

    def request(self, transport):
        self.log.debug(r'Sending NVER\n')
        transport.write('NVER\n')

#####################

class WAKEUP(StandardCommand):
    def __init__(self, protocol):
        StandardCommand.__init__(self, protocol)
        self.wakeupTries = 0

    def execute(self, response):
        self.wakeupTimerID.cancel()
        self.protocol.woken_up()
        self.client.on_wakeup(self.protocol)

    def request(self, transport):    
        """Send a \n and set a timer for 1.2s later to send another \n"""
        self.log.debug('Sending wakeup')
        self.wakeupTries = self.wakeupTries + 1
        if (self.wakeupTries > 30):
            self.log.error('Failed to get a response from the device')
            reactor.stop()
        else:
            transport.write(b'\n')
            self.wakeupTimerID = reactor.callLater(1.2, lambda: self.request(transport))

    def handle_data(self, buff):
        if (buff.find('\n\r') < 0):
            return (buff, VantageCommandState.NOT_ENOUGH_DATA)
        else:
            self.execute('')
            return (buff[2:], VantageCommandState.DONE)

#####################

class VantageProProtocolHandler(twisted.protocols.basic.LineReceiver):
    ack    = chr(0x06)
    resend = chr(0x15) # NB: Davis docs say 0x21, but they mean 21
    crc_failed = chr(0x18)
    nack = chr(0x21)

    def __init__(self, client):
        # We receive data terminated with \n\r and we send it terminated with \n
        self.log = logging.getLogger('raw_logger')
        self.setRawMode()
        self.client = client
        self.command = None

        self.reset()

    def request_firmware_date(self):
        self.log.debug('Sending VER')
        self.command = VER(self)
        self.command.request(self.transport)

    def request_firmware_version(self):
        self.log.debug('Sending NVER')
        self.command = NVER(self)
        self.command.request(self.transport)

    def request_test(self):
        self.log.debug('Sending TEST')
        self.command = TEST(self)
        self.command.request(self.transport)

    def request_readings(self):
        self.log.debug('Sending LOOP')
        self.command = LOOP(self)
        self.command.request(self.transport)

    def reset(self):
        """ Reset the protocol state """
        self.state = VantageProtocolState.ASLEEP
        self.buff = ''
        self.command = None

    ####### Implementation #########     

    def wake_up_device(self):
        #Console Wakeup procedure:
        #1. Send a Line Feed character, \n (decimal 10, hex 0x0A).
        #2. Listen for a returned response of Line Feed and Carriage Return characters, (\n\r).
        #3. If there is no response within a reasonable interval (say 1.2 seconds), then try steps 1 and
        #2 again up to a total of 3 attempts.
        #4. If the console has not woken up after 3 attempts, then signal a connection error
        self.state = VantageProtocolState.WAKINGUP
        self.command = WAKEUP(self)
        self.command.request(self.transport)

    def woken_up(self):
        if (self.state == VantageProtocolState.WAKINGUP):
            self.state = VantageProtocolState.OK_TO_SEND

    def makeHumanReadable(self, data):        
        return data.__repr__()

    # LineRecevier overrides
    def rawDataReceived(self, data):
        """ Callback from twisted, I don't think we can assume we'll 
            have a complete line of data here, so we won't. """

        self.log.debug("GOT %s" % self.makeHumanReadable(data))

        if (self.command is None):
            self.log.debug('Unsolicited data arrived!')
            reactor.stop()

        # We'll accumulate the data we've been given into a buffer
        self.buff += data

        ### What do we do about looping on a full buffer, where we don't want to 
        ### loop indefinitely on a buffer with a partial command.
        ### Case 1: We read the whole buffer and use it. 
        ### Case 2: We read part of the buffer, and want to be called again as there
        ###         is stuff in the buffer. 
        ### Case 3: We read the whole buffer, but it's not useful so we need more data from the port. 

        ### DONE, NOT_ENOUGH, MORE

        while (True):
            self.log.debug('PROCESSING: %s' % self.makeHumanReadable(self.buff))
            (self.buff, command_state) = self.command.handle_data(self.buff)

            # Case 1. There wasn't enough data in the buffer for the command
            #         so we need to wait for more data
            if (command_state == VantageCommandState.NOT_ENOUGH_DATA):
                return

            # Case 2. The command is done. We can return and send another one.
            if (command_state == VantageCommandState.DONE):
                self.command = None
                self.state = VantageProtocolState.OK_TO_SEND
                return

            # Case 3. The command isn't done, so we can try it again with whatever
            #         is left in the buffer.
            if (command_state == VantageCommandState.AGAIN):
                pass

    # Transport overrides
    def connectionMade(self):
        """ When the connection is made we initiate the wakeup protocol """
        self.wake_up_device()

    def connectionLost(self, reason):
        """ When the connection is lost we tear down the reactor to 
        the calling application will exit. """

        self.log.debug('Got connection lost: stopping event loop')
        self.state = VantageProtocolState.STOPPED
        try:
            reactor.stop()
        except:
            pass
