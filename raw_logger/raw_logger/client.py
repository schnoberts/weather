from twisted.internet import reactor

class Client:
    def __init__(self):
        self.protocol = None
        self.stage = 0
        self.dump = open('loop.log', 'w')
        self.count = 0

    def on_wakeup(self, p):
        print 'Woken Up'
        print 'Scheduling TEST in 2s'
        self.protocol = p
        reactor.callLater(2, self.find_stuff_out)

    def find_stuff_out(self):    
        if (self.stage == 0):
            self.protocol.request_test()
            print 'Scheduling VER in 2s'
            reactor.callLater(2, self.find_stuff_out)
            self.stage = 1
        elif (self.stage == 1):
            self.protocol.request_firmware_date()
            print 'Scheduling LOOP in 2s'
            reactor.callLater(2, self.find_stuff_out)
            self.stage = 2
        else:
            self.protocol.request_readings()

    def got_reading(self, p, reading):
        self.count += 1
        print '%d: Got reading of %d' % (self.count, len(reading))
        self.dump.write(reading)
        self.dump.flush()
        if (self.count >= 10):
            reactor.stop()

    def got_ack(self, p, c):
        print 'Got ACK for %s'%c
        pass

    def got_nack(self, p, c, nack):
        p.request_firmware_version()

    def got_ver(self, p, firmware_date):
        print 'FW Date', firmware_date

    def got_nver(self, p, firmware_version):
        print 'FW', firmware_version

    def got_test(self, p, response):
        print 'TEST', response
