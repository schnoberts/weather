#TEST = True
TEST = False

if (TEST):
    import mockserial

import vantage
import time
import client

from twisted.internet import serialport, reactor

import logging
import optparse
import os

def runServer(mock):
    time.sleep(3)
    count = 0
    while (True):
        line = ''
        line = mock.read(1000)
        print ('Read: [%s]' % line)
        if line == '\n':
            print(r'Sending \n\r to slave')
            mock.write(b'\n\r')
        elif line == 'VER\n':
            print(r'Sending OK\n\r')
            mock.write(b'\n\rOK\n\rApr')
            time.sleep(0.5)
            mock.write(b' 24 2002\n\r')
        elif line == 'NVER\n':
            print(r'Sending OK\n\r')
            mock.write(b'\n\rOK\n\r"1.73"\n\r')
        elif line == 'TEST\n':
            print(r'Sending TEST response\n\r')
            mock.write(b'TEST\n\r')
        elif line == 'LOOP 10\n':
            mock.write(vantage.ACK)
            buff = ''.join([chr(x) for x in range(0, 99)])
            for i in range(0, 10):
                mock.write(buff)

def main():
    parser = optparse.OptionParser() 
    parser.add_option('-p', '--port', help='serial port', default='/dev/ttyS0')
    parser.add_option('-b', '--baud', help='baud rate', default='19200')

    (options, args) = parser.parse_args()

    logging.basicConfig()
    log = logging.getLogger('raw_logger')
    log.setLevel(logging.DEBUG)

    port = options.port
    baud = int(options.baud)

    log.debug('Opening %s at %d baud' % (port, baud))
    protocol_handler = vantage.VantageProProtocolHandler(client.Client())
    p = serialport.SerialPort(protocol_handler, port, reactor, baudrate = baud)

    if (TEST):
        log.debug('Fork test driver')
        pid = os.fork()

        if (pid == 0):
            mockserial.MockSerial.mock.master()
            runServer(mockserial.MockSerial.mock)
        else:
            mockserial.MockSerial.mock.slave()

            log.debug('Starting event loop')

            reactor.run()

            log.debug('Event loop stopped')
            log.debug('Stopping child')

            os.kill(pid, 9)
    else:
        reactor.run()
        log.debug('Event loop stopped')

if __name__ == '__main__':
    main()
