import mock
import serial
import os
import socket

class MockSerial:
    mock = None
    def __init__(self, deviceNameOrPortNumber, baudrate, bytesize,
                 parity, stopbits, timeout, xonxoff, rtscts):

        MockSerial.mock = self

        (self.s, self.m) = socket.socketpair()
        self.slaveFD = self.s.fileno()
        self.masterFD = self.m.fileno()

        self.fd = self.slaveFD
        self.mode = 'S'

    def master(self):    
        #os.close(self.slaveFD)
        self.fd = self.masterFD
        self.mode = 'M'

    def slave(self):    
        #os.close(self.masterFD)
        self.fd = self.slaveFD
        self.mode = 'S'

    def close(self):
        os.close(self.fd)
        pass

    def fileno(self):
        return self.fd

    def read(self, size = 1):
        return os.read(self.fd, size)

    def write(self, line):
        return os.write(self.fd, line)

    def flush(self):
        pass

    def flushInput(self):
        pass

    def flushOutput(self):
        pass


serial.Serial = MockSerial

