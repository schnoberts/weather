from setuptools import setup, find_packages

setup (	name = 'raw_logger',
	version = '1.0', 
	packages = find_packages(),
        install_requires = ['twisted', 'nose', 'mock', 'pyserial']);

